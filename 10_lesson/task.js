'use strict';

function getThemeList(tutorialPart, tutorialChapter) {
    validationOfInput(tutorialPart, tutorialChapter);
    return document.querySelectorAll('body > div.page-wrapper > ' +
        'div.page.page_contains_header.page_sidebar-animation-on > div.page__inner > main > div.content.frontpage > div > ' +
        'section:nth-child(' + tutorialPart + ') > div > div.list > div:nth-child(' + tutorialChapter + ') > ul > li > div > a');


}


function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function validationOfInput(...inputs) {
    for (let i = 0; i < 2; i++) {
        if (!inputs[i] && inputs[i] !== 0) {
            return alert('you input not all parameters')

        } else if (inputs[i] === 0) {
            return alert('Please input numbers bigger than 0. Error in ' + (i + 1) + ' argument');

        } else if (!isNumeric(inputs[i]) || inputs[i] <= 0) {
            return alert('Your input is invalid \n Please input plural numbers')

        }
    }
    if (inputs[0] > 3) {
        return alert('We have only 3 chapters')
    }

}

function outputThemeList(tutorialPart, tutorialChapter, outputType) {
    let themeList = getThemeList(tutorialPart, tutorialChapter);
    if (outputType === 'current') {
        for (let i = 0; i < themeList.length; i++) {
            console.log(tutorialChapter + '.' + (i + 1) + ' ' + themeList[i].innerText)
        }
    } else if (outputType === 'number') {

        for (let i = 0; i < themeList.length; i++) {
            console.log((i + 1) + ' ' + themeList[i].innerText)
        }
    } else if (outputType === 'dash') {

        for (let i = 0; i < themeList.length; i++) {
            console.log('-- ' + themeList[i].innerText)
        }
    } else {
        console.warn('outputType incorrect')
    }
}

